class ReservationMailer < ApplicationMailer
  default from: "indiancafedelhi@gmail.com"

  def receive_reservation_email(reservation)
    @reservation = reservation
    mail(to: "indiancafedelhi@gmail.com", subject: "Есть бронирование это круто")
  end
end

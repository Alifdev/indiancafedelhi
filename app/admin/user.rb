ActiveAdmin.register User do
  permit_params :name, :email, :password, :password_confirmation, role_ids: []

  index do
    column :id
    column :email
    column :first_name
    column :middle_name
    column :last_name
    column :roles do |user|
      user.roles.collect(&:name).to_sentence
    end
    column :sign_in_count
    column :current_sign_in_at
    column :last_sign_in_at
    actions
  end

  form do |f|
    f.inputs 'User Details' do
      f.input :first_name
      f.input :middle_name
      f.input :last_name
      f.input :roles, as: :check_boxes, multiple: false, collection: Role.all
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end
end

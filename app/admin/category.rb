ActiveAdmin.register Category do
  config.per_page = 10

  permit_params :id, :name

  index do
    column :id
    column :name
    actions
  end
end

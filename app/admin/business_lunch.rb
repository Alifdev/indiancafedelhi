ActiveAdmin.register BusinessLunch do
  config.per_page = 20

  # filter :day_id, as: :select, collection: Date::DAYNAMES.zip((0..7).to_a)
  # filter :created_at, as: :date_range

  permit_params food_ids: []

  index do
    column :id
    column :food_ids, sortable: 'food_ids'
    actions
  end


  show do
    panel 'Info' do
      table_for business_lunch do
        column :id
        column :created_at
        column :updated_at
      end
    end

    panel 'Foods' do
      table_for business_lunch.foods.order(:category_id) do |_category|
        column :category
        column :name
        column :price
      end
    end
  end

  form do |f|
    f.inputs 'Business Lunch' do
      Category.all.where(id: [174, 173, 172, 171]).each do |c|
        f.input :foods, label: "Foods [ #{c.name} ]", as: :check_boxes, collection: Food.where(category: c)
      end
    end
    f.actions
  end
end

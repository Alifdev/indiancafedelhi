ActiveAdmin.register Reservation do
  config.per_page = 10

  permit_params :id, :group_size, :date, :time, :table_type, :mobile_number

  index do
    column :id
    column :group_size
    column :date
    column :time
    column :table_type
    column :mobile_number

    actions
  end
end

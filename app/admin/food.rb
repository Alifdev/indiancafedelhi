ActiveAdmin.register Food do
  config.filters = false
  config.per_page = 25

  permit_params :category_id, :name, :price, :details, :quantity

  index do
    column :id
    column :name
    column :category, sortable: 'category_id'
    column :price
    column :details
    column :quantity
    column :created_at
    column :updated_at
    actions
  end

end

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require vendor/jquery-1.12.0.min

//= require toastr_rails
//= require bootstrap-datepicker
//= require_tree .

//= require vendor/modernizr-2.8.3.min
//= require vendor/fastclick
//= require vendor/jff-vendor.min
//= require plugins/slick.min
//= require plugins/select2.min
//= require plugins/jquery-ui.min
//= require plugins/imagesloaded.pkgd.min
//= require plugins/isotope.pkgd.min
//= require plugins/jquery.magnific-popup.min
//= require plugins/jquery.countdown.min
//= require plugins/bootstrap-timepicker.min
//= require plugins/extensions/jquery.themepunch.tools.min
//= require plugins/extensions/jquery.themepunch.revolution.min
//= require plugins/bootstrap.min
//= require main
//= require plugins/extensions/revolution.extension.layeranimation.min
//= require plugins/extensions/revolution.extension.kenburn.min
//= require plugins/extensions/revolution.extension.navigation.min
//= require plugins/extensions/revolution.extension.parallax.min
//= require plugins/extensions/revolution.extension.video.min
//= require plugins/extensions/revolution.extension.slideanims.min


$(document).ready(function() {
  $("#datepicker").datepicker({
    dateFormat: 'dd-mm-yy'
  });

  toastr.options = {
    "closeButton": false,
    "debug": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

});


# == Schema Information
#
# Table name: business_lunches
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BusinessLunch < ApplicationRecord
  has_many :food_associations, as: :food_association, dependent: :destroy
  has_many :foods, through: :food_associations

  validates :foods, presence: true
end

# == Schema Information
#
# Table name: reservations
#
#  id            :integer          not null, primary key
#  group_size    :integer
#  date          :date
#  time          :datetime
#  table_type    :string
#  mobile_number :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  name          :string
#

class Reservation < ApplicationRecord
  TableTyes =  ["Группа - 8 гостей", "Классик - 5 гостей", "Стандарт - 4 гостя", "Больше 8"]
  validates :name, :group_size, :date, :time, :table_type, :mobile_number, presence: true
end

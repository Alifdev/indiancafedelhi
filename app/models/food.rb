# == Schema Information
#
# Table name: foods
#
#  id          :integer          not null, primary key
#  name        :string
#  details     :string
#  price       :string
#  quantity    :string
#  category_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_foods_on_category_id  (category_id)
#
# Foreign Keys
#
#  fk_rails_...  (category_id => categories.id)
#

class Food < ApplicationRecord
  belongs_to :category
  has_many  :food_associations, dependent: :destroy

  validates :category, presence: true
  validates :name, presence: true, uniqueness: true
  validates :price, presence: true
  validates :quantity, presence: true
end

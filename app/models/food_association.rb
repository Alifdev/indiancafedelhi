# == Schema Information
#
# Table name: food_associations
#
#  id                    :integer          not null, primary key
#  price                 :decimal(, )
#  food_id               :integer
#  food_association_id   :integer
#  food_association_type :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class FoodAssociation < ApplicationRecord
  before_create :set_price

  belongs_to :food
  belongs_to :food_association, polymorphic: true

  def set_price
    self.price = food.price
  end
end

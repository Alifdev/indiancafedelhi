class StaticPagesController < ApplicationController
  def home
  end

  def contact
  end

  def commingsoon
  end

  def index
    @reservation = Reservation.new
    @foods_decerts = Food.all.where(category_id: [43, 44, 45, 47]).limit(10) # all id from heroku db
    @foods_mix = Food.all.where(category_id: [31, 34, 31]).limit(10) # all id from heroku db
    @business_lunch = Category.all.where(name: ["Гарнир на выбор", "Горячее на выбор", "Супы на выбор", "Салат на выбор"])
  end

  def new_index
  end

  def menu
    @categories = Category.all
    @foods = Food.all
  end
end

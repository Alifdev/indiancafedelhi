class ReservationsController < ApplicationController
  def new
    @reservation = Reservation.new
  end

  def create
    @reservation = Reservation.create(reservation_params)
    if @reservation.save
      ReservationMailer.receive_reservation_email(@reservation).deliver_now
      redirect_to root_url, notice: "Ваше бронирование подтверждено. Спасибо"
    else
      redirect_to root_url, error: "Все поля обязательны для заполнения."
    end
  end

  private
  def reservation_params
    params.require(:reservation).permit(:id, :name, :group_size, :date, :time, :table_type, :mobile_number)
  end
end

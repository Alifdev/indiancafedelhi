class CreateReservations < ActiveRecord::Migration[5.1]
  def change
    create_table :reservations do |t|
      t.integer :group_size
      t.date :date
      t.datetime :time
      t.string :table_type
      t.string :mobile_number

      t.timestamps
    end
  end
end

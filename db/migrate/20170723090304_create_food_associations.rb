class CreateFoodAssociations < ActiveRecord::Migration[5.1]
  def change
    create_table :food_associations do |t|
      t.decimal :price
      t.integer :food_id
      t.integer :food_association_id
      t.string :food_association_type

      t.timestamps
    end
  end
end

class CreateFoods < ActiveRecord::Migration[5.1]
  def change
    create_table :foods do |t|
      t.string :name
      t.string :details
      t.string :price
      t.string :quantity
      t.references :category, index: true, foreign_key: true

      t.timestamps
    end
  end
end

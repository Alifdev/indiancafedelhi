# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# User.create!(email: 'admin@admin.com', password: 'password', password_confirmation: 'password')


categories = Category.create([
{ name: 'Салаты' }, #0
{ name: 'Холодные закуски' }, # 1
{ name: 'Горячие закуски' }, # 2
{ name: 'Супы' }, # 3
{ name: 'Вегетарианские горячие блюда' }, # 4
{ name: 'Блюда из цыпленка' }, # 5
{ name: 'Блюда из ягненка' }, # 6
{ name: 'Блюда из рыбы и морепродуктов' }, # 7
{ name: 'Блюда из тандура' }, # 8
{ name: 'Лепешки из тандура' }, # 9
{ name: 'Плов по индийским традициям' },  #10
{ name: 'Гарниры' },  #11
{ name: 'Соусы' },  #12
{ name: 'Индийские горячие напитки' },  #13
{ name: 'Кофе' },  #14
{ name: 'Индийские холодные напитки' },  #15
{ name: 'Индийские десерты' },  #16
{ name: 'Десерты' },  #17
{ name: 'Мороженое' },  #18
{ name: 'Свежевыжатые соки' },  #19
{ name: 'Молочные коктейли' },  #20
{ name: 'Салат на выбор' }, #21
{ name: 'Супы на выбор' }, #22
{ name: 'Горячее на выбор' }, #23
{ name: 'Гарнир на выбор'}, #24
{ name: 'Гарнир на Напитки'}, #25
])

салаты = Food.create!([
  { name: 'Мург Чат ',    price: "290", details: "", quantity: "250", category: categories[0] },
  { name: 'Чана',         price: "230", details: "", quantity: "230", category: categories[0] },
  { name: 'Баклажаны в кисло-сладкоим соусе ', price: "270", details: "", quantity: "270", category: categories[0] },
  { name: 'Овощной салат ', price: "220", details: "", quantity: "200", category: categories[0] },
  { name: 'Греческий', price: "250", details: "", quantity: "250", category: categories[0] },
  { name: 'Цезарь с куриным филе ', price: "270", details: "", quantity: "200", category: categories[0] },
  { name: 'Цезарь с креветками ', price: "330", details: "", quantity: "200", category: categories[0] },
  { name: 'Салат с морепродуктами от Шеф-повара ', price: "350", details: "", quantity: "250", category: categories[0] },
  { name: 'Дели', price: "370", details: "", quantity: "200", category: categories[0] }
])

холодные_закуски = Food.create!([
{ name: 'Сырная тарелка ', price: "390", details: "", quantity: "210", category: categories[1] },
{ name: 'Фруктовая тарелка ', price: "320", details: "", quantity: "350", category: categories[1] },
{ name: 'Чипсы ', price: "140", details: "", quantity: "120", category: categories[1] }
])

горячие_закуски = Food.create!([
{ name: 'Гренки с сыром и чесноком ', price: "140", details: "", quantity: "250", category: categories[2] },
{ name: 'Овощная Самоса ', price: "220", details: "", quantity: "250", category: categories[2] },
{ name: 'Кима Самоса ', price: "290", details: "", quantity: "250", category: categories[2] },
{ name: 'Панир Пакора', price: "280", details: "", quantity: "250", category: categories[2]},
{ name: 'Овощная Пакора', price: "250", details: "", quantity: "250", category: categories[2] },
{ name: 'Фиш Пакора', price: "290", details: "", quantity: "250", category: categories[2] },
{ name: 'Чикен Пакора', price: "290", details: "", quantity: "250", category: categories[2] }
])

супы = Food.create!([
{ name: 'томатный суп', price: "180", details: "", quantity: "250", category: categories[3] },
{ name: 'Дал суп', price: "180", details: "", quantity: "250", category: categories[3] },
{ name: 'Овощной суп', price: "180", details: "", quantity: "250", category: categories[3] },
{ name: 'Овощной крем-суп', price: "190", details: "", quantity: "250", category: categories[3]},
{ name: 'Суп из морепродуктов', price: "260", details: "", quantity: "300", category: categories[3] },
{ name: 'Куриный суп с лапшой', price: "150", details: "", quantity: "250", category: categories[3] },
{ name: 'Крем суп из шпината', price: "250", details: "", quantity: "250", category: categories[3] }
])

Вегетерианские_горячие_блюда = Food.create!([
{ name: ' Дал Макхани', price: "250", details: "", quantity: "250", category: categories[4] },
{ name: ' Дал Тардка', price: "230", details: "", quantity: "250", category: categories[4] },
{ name: ' Матар Панир', price: "350", details: "", quantity: "250", category: categories[4] },
{ name: ' Овощной микс', price: "290", details: "", quantity: "250", category: categories[4]},
{ name: ' Палак Панир', price: "380", details: "", quantity: "250", category: categories[4] },
{ name: ' Малай Кофта', price: "390", details: "", quantity: "270", category: categories[4] }
])


Блюда_из_цыпленка = Food.create!([
{ name: 'Чикен карри', price: "350", details: "", quantity: "270", category: categories[5] },
{ name: 'Кадай Чикен', price: "370", details: "", quantity: "270", category: categories[5] },
{ name: 'Чикен Тикка Масала', price: "370", details: "", quantity: "270", category: categories[5] },
{ name: 'Батар Чикен', price: "390", details: "", quantity: "270", category: categories[5]},
{ name: 'Сааг Чикен', price: "390", details: "", quantity: "270", category: categories[5] },

])

Блюда_из_ягненка = Food.create!([
{ name: 'Рогон Джос', price: "460", details: "", quantity: "270", category: categories[6] },
{ name: 'Сааг Гост', price: "480", details: "", quantity: "270", category: categories[6] },
{ name: 'Кодай Гост', price: "460", details: "", quantity: "270", category: categories[6] }
])

Блюда_из_рыбы_и_морепродуктов = Food.create!([
{ name: 'Фиш Карри ', price: "350", details: "", quantity: "270", category: categories[7] },
{ name: 'Гоа Фиш Карри ', price: "430", details: "", quantity: "270", category: categories[7] },
{ name: 'Джинга Малай Карри ', price: "650", details: "", quantity: "270", category: categories[7] }
])

Блюда_из_тандура = Food.create!([
{ name: 'Тандури Чикен ', price: "490", details: "", quantity: "400", category: categories[8] },
{ name: 'Гарлик Чикен Тикка ', price: "350", details: "", quantity: "220", category: categories[8] },
{ name: 'Мург Малай Тикка ', price: "350", details: "", quantity: "220", category: categories[8] },
{ name: 'Панир Тикка ', price: "490", details: "", quantity: "250", category: categories[8]},
{ name: 'Тандури Морской Окунь от Шеф-повара ', price: "420", details: "", quantity: "230", category: categories[8] },
{ name: 'Гост Бати Кебаб ', price: "450", details: "", quantity: "180", category: categories[8] },
{ name: 'Сик Кебаб ', price: "480", details: "", quantity: "180", category: categories[8] },
{ name: 'Овощи в Тандуре ', price: "320", details: "", quantity: "250", category: categories[8] },
{ name: 'Утка из тандура ', price: "450", details: "", quantity: "150", category: categories[8] }
])

Лепешки_из_тандура = Food.create!([
{ name: 'Тандури Чапати ', price: "50", details: "", quantity: "100", category: categories[9] },
{ name: 'Нан Тандури с кунжутом ', price: "70", details: "", quantity: "110", category: categories[9]},
{ name: 'Баттер Нан со сливочным маслом ', price: 80, details: "", quantity: "110", category: categories[9] },
{ name: 'Алу Нан с картофелем ', price: "150", details: "", quantity: "180", category: categories[9] },
{ name: 'Панир Кульча с адыгейским сыром и зеленью ', price: "180", details: "", quantity: "100", category: categories[9] },
{ name: 'Чиз Гарлик Нан с сыром и чесноком ', price: "190", details: "", quantity: "180", category: categories[9] },
{ name: 'Гарлик Нан с чесноком ', price: "120", details: "", quantity: "130", category: categories[9] }
])


Плов_по_индийским_традициям = Food.create!([
{ name: 'Овощной Берияни', price: "380", details: "", quantity: "350", category: categories[10] },
{ name: 'Чикен Берияни', price: "420", details: "", quantity: "350", category: categories[10]},
{ name: 'Гост Берияни', price: "520", details: "", quantity: "350", category: categories[10] }
])

Гарниры = Food.create!([
{ name: 'Индийский рис Басмати', price: "150", details: "", quantity: "250", category: categories[11] },
{ name: 'Цветной рис Басмати', price: "170", details: "", quantity: "250", category: categories[11]},
{ name: 'Лимонный рис', price: "220", details: "", quantity: "250", category: categories[11] },
{ name: 'Картофель Фри', price: "150", details: "", quantity: "110", category: categories[11] },
{ name: 'Картофель Айдахо', price: "150", details: "", quantity: "150", category: categories[11] }

])

Соусы = Food.create!([
{ name: 'Томариновый',       price: "50", details: "", quantity:"0", category: categories[12] },
{ name: 'Тар-Тар',           price: "50", details: "", quantity:"0", category: categories[12] },
{ name: 'Кетчуп',            price: "50", details: "", quantity:"0", category: categories[12] },
{ name: 'Сырный',            price: "50", details: "", quantity:"0", category: categories[12] },
{ name: 'Индийский острый ', price: "50", details: "", quantity:"0", category: categories[12] }
])

Индийские_горячие_напитки = Food.create!([
{ name: 'Масала Чай ',       price: "150/280",  details: "",  quantity: "500/1000 мл", category: categories[13] },
{ name: 'Имбирный напиток ', price: "150/280", details: "", quantity: "500/1000 мл", category: categories[13]}

])

Кофе = Food.create!([
{ name: 'Эспрессо ', price: "90", details: "",    quantity:"0", category: categories[14] },
{ name: 'Американо', price: "120", details: "",   quantity:"0", category: categories[14]},
{ name: 'Капучино ', price: "150", details: "",   quantity:"0", category: categories[14] },
{ name: 'Гляссе ',   price: "200", details: "",   quantity:"0", category: categories[14] }

])

Индийский_холодные_напитки = Food.create!([
{ name: 'Манго Ласси ',   price: "200", details: "", quantity: "200", category: categories[15] },
{ name: 'Свит Ласси ',    price: "150", details: "", quantity: "200", category: categories[15]},
{ name: 'Соленый Ласси ', price: "150", details: "", quantity: "200", category: categories[15] }
])

Индийские_десерты = Food.create!([
{ name: 'Глаб Джамун ', price: "180", details: "", quantity: "110", category: categories[16] },
{ name: 'Гаджар Халва ', price: "220", details: "", quantity: "150", category: categories[16]}
])

Десерты = Food.create!([
{ name: 'Чизкейк классический', price: "170", details: "", quantity: "100", category: categories[17] },
{ name: 'Чизкейк шоколадный', price: "180", details: "", quantity: "100", category: categories[17]}

])
Мороженое = Food.create!([
{ name: 'Ванильное', price: "180", details: "", quantity: "150", category: categories[18] },
{ name: 'Шоколадное', price: "180", details: "", quantity: "150", category: categories[18]},
{ name: 'Фисташковое', price: "180", details: "", quantity: "150", category: categories[18] },
{ name: 'Манго', price: "180", details: "", quantity: "159", category: categories[18] }
])

Свежевыжатые_соки = Food.create!([
{ name: 'Яблоко-морковь', price: "200", details: "", quantity: "200", category: categories[19] },
{ name: 'Апельсин', price: "200", details: "", quantity: "200", category: categories[19]},
{ name: 'Грейпфрут', price: "200", details: "", quantity: "200", category: categories[19] }

])

Молочные_коктейли = Food.create!([
{ name: 'Клубничный', price: "200", details: "", quantity: "300", category: categories[20] },
{ name: 'Ванильный', price: "200", details: "", quantity: "300", category: categories[20]},
{ name: 'Шоколадный', price: "200", details: "", quantity: "300", category: categories[20] }
])

Салат_на_выбор = Food.create([
{ name: 'Овощной салат',    price: "0", details: "", quantity: "0", category: categories[21] },
{ name: 'Салат Тастана',    price: "0", details: "", quantity: "0", category: categories[21] },
{ name: 'Салат недели',    price: "0", details: "", quantity: "0", category: categories[21] },
])

Супы_на_выбор = Food.create!([
{ name: 'Суп овощной',    price: "0", details: "", quantity: "0", category: categories[22] },
{ name: 'Суп дня',    price: "0", details: "", quantity: "0", category: categories[22] }
])


Горячее_на_выбор = Food.create!([
{ name: 'Куриный Шашлык',    price: "0", details: "", quantity: "0", category: categories[23] },
{ name: 'Фисш Кари',    price: "0", details: "", quantity: "0", category: categories[23] },
{ name: 'Тандури – Кебаб  из домашнего фарша',    price: "0", details: "", quantity: "0", category: categories[23] },
{ name: 'Спагетти Карбонара ',    price: "0", details: "", quantity: "0", category: categories[23] },
])

Гарнир_на_выбор = Food.create!([
{ name: 'Индийский рис ',    price: "0", details: "", quantity: "0", category: categories[24] },
{ name: 'Картофельное пюре ',    price: "0", details: "", quantity: "0", category: categories[24] },
{ name: 'Овощной микс',    price: "0", details: "", quantity: "0", category: categories[24] },
])

Супы_на_выбор = Food.create!([
{ name: 'ЧАЙ ИЛИ КОФЕ ',    price: "0", details: "", quantity: "0", category: categories[22] },
{ name: 'МОРС',    price: "0", details: "", quantity: "0", category: categories[22] }
])










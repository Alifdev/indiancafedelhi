Rails.application.routes.draw do
  resources 'reservations'

  ActiveAdmin.routes(self)
  devise_for :users
  #root 'static_pages#index'
  root 'static_pages#index'
  # get 'contact', to: 'static_pages#contact'
  get 'commingsoon', to: 'static_pages#commingsoon'
  get 'newindex', to: 'static_pages#new_index'
  get 'menu', to: 'static_pages#menu'
  get '/robots.txt' => 'robots_txts#disallow_all'
end
